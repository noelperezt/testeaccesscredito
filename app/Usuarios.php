<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    protected $fillable = [
        'nome',
        'cpf',
        'data_nac',
        'email',
        'telefone',
        'endereco',
        'bairro',
        'numero',
        'complemento',
        'cidade',
        'estado',
        'cep',
    ];
}
