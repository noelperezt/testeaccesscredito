<?php

namespace App\Http\Controllers;

use App\Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = DB::table('usuarios')->get();
        return response()->json([
            'usuarios'    => $usuarios,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome'=> 'required',
            'cpf' => 'required',
            'data_nac' => 'required|date|',
            'email' => 'required|email|',
            'numero' => 'required',
            'telefone' => 'required',
            'endereco' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'estado' => 'required',
            'cep' => 'required'
        ]);
 
        $usuario = Usuarios::create([
            'nome' => request('nome'),
            'cpf'=> request('cpf'),
            'data_nac'=> request('data_nac'),
            'email'=> request('email'),
            'telefone'=> request('telefone'),
            'endereco'=> request('endereco'),
            'bairro'=> request('bairro'),
            'numero'=> request('numero'),
            'complemento'=> request('complemento'),
            'cidade'=> request('cidade'),
            'estado' => request('estado'),
            'cep' => request('cep')
        ]);
 
        return response()->json([
            'usuario'    => $usuario,
            'message' => 'Success'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show(Usuarios $usuarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit(Usuarios $usuarios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome'=> 'required',
            'cpf' => 'required',
            'data_nac' => 'required|date|',
            'email' => 'required|email|',
            'telefone' => 'required',
            'numero' => 'required',
            'endereco' => 'required',
            'cidade' => 'required',
            'estado' => 'required',
            'cep' => 'required'
        ]);

        $usuario = $request -> all();
        Usuarios::find($id) -> update($usuario);

        return response()->json([
            'message' => 'Task updated successfully!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuarios::findOrFail($id);
        $usuario->delete();

        return response()->json([
            'message' => 'Task deleted successfully!'
        ], 200);
    }
}
