<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('cpf', 15);
            $table->date('data_nac');
            $table->string('email');
            $table->string('telefone', 20);
            $table->string('endereco');
            $table->string('bairro');
            $table->integer('numero');
            $table->string('complemento')->nullable();;
            $table->string('cidade');
            $table->string('estado');
            $table->string('cep',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
